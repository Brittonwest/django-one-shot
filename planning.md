* [x] Fork and clone the starter project from django-one-shot 
  * [x] fork project into personal git
  * [x] set public
  * [x] clone
  * [x] cd projects
  * [x] git clone {link}
  * [x] cd into clone
  * [x] open VSC code .
* [x] Create a new virtual environment in the repository directory for the project
  * [x] python -m venv .venv
* [x] Activate the virtual environment
  * [x] source .venv/bin/activate
* [x] Upgrade pip
  * [x] python -m pip install --upgrade pip
* [x] Install django
  * [x] pip install django
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8
* [x] Install djlint
  * [x] pip install djlint
* [x] Deactivate your virtual environment
  * [x] deactivate
* [x] Activate your virtual environment
  * [x] source .venv/bin/activate
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt
* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
  * [x] django-admin startproject brain_two
* [x] Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
  * [x] django-admin startapp todos
* [x] Run the migrations
  * [x] python manage.py makemigrations
  * [x] python manage.py migrate
* [x] Create a super user
  * [x] python manage.py createsuperuser
    * [x] britton
    * [x] brittjamesw@gmail.com
    * [x] 123
    * [x] 123
* [x] add / commit changes
  * [x] gid add .
  * [x] git commit -m"Feature 2 Complete"
  * [x] git push origin main
* [x] in Models create TodoList
  * [x] class 
  * [x] name = models.
  * [x] created_on = models.DateTimeField(auto_now_add=True)
  * [x] Self Define
* [x] insert todos into settings.py 
* [x] INSTALLED_APPS -> todos.apps.TodosConfig [apps.py reference] 
* [ERROR-APPS-FIX] /var/folders/2x/hs0nmc654432q728wr10qc440000gn/T/TemporaryItems/NSIRD_screencaptureui_OC44wB/Screen Shot 2022-07-26 at 8.56.58 PM.png
* [x] Make Migrations / Migrate
* [x] Run Django shell
  * [x] python manage.py shell
* [x] Import Todolist.objects.create
  * [x] todolist = TodoList.objects.create(name="reminders")
* [x] Git add, commit and push   
* [x] Register TodoList in Admin.py
* [x] git / commit / push
* [ ] create a TodoItem model in the todos Django app
  * [x] Task String = CharField max 100
  * [x] due_date = optional (models.DateTimeField(blank=True, null=True))
  * [x]  is_completed = models.BooleanField(defualt=False)
  * [x] list = ForeignKey -> TodoList (related name) (on delete = cascade)
  * [x] self define __str__(self): -> return
  * [x] makemigrations / migrate
  * [x] test in shell
    * [x] python manage.py shell
    * [x] from todos.models import TodoList,TodoItem
    * [x] todolist = TodoList.objects.first()
    * [x] todoitem = TodoItem.objects.create(task="Take out the Garbage", list=todolist)
* [x] git add commit push 
* [x] register TodoItem in Admin.py
* [x] git add commit push
* [ ] Create view for TodoList
  * [x] def todo_list_list(request)
  * [x] todolist = [CLASS].objects.all()
  * [x] context = {"": }
  * [x] return / render / path / context
* [x] register view in urls
  * [x] create urls.py in todos
  * [x] from django.contrib import admin
  * [x] from django.urls import include, path
  * [x] from todos.views import [view]
  * [x] create urlpatterns = []
  * [x] path todo_list_list
* [ ] include urls in brain_two project (prefix todos/)
* [x] create html template for list.view
  * [x] html:5
  * [x] main tag
  * [x] div tag
  * [x] h1 tag "my lists"
  * [x] div tag -> create table
    * [x] <table> / <thead> / <tr> / <th>
  * [x] table with two columns
    * [x] 1st header "name"
    * [x] 2nd header "number of items"
* [x] create view for TodoList with name field in the form
  * [x] create a forms.py
  * [x] model is linked to .models
  * [x] form data is "name"
  * [x] import .models
  * [x] go to .views
  * [x] import redirect
  * [x] from .forms import TodoForm
  * [x] create todo_list_create
    * [x] context = {}
    * [x] define form with request.POST
    * [x] if statement to validate form
    * [x] form.save()
    * [x] return redirect
    * [x] context for form
    * [x] return render create.html
* [x] todo-list should redirect to detail page
* [x] register path "create/" in urls
* [x] create html create 
* [x] git add / commit / push main 