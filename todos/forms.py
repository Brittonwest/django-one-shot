from django import forms
from .models import TodoItem, TodoList


class TodoForm(forms.ModelForm):
    class Meta:
        model = TodoList

        fields = [
            "name",
        ]


class ItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem

        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
