from django.shortcuts import render
from django.db import models
from django.db.models.deletion import CASCADE


class TodoList(models.Model):
    name = models.CharField(max_length=100, null=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(blank=True, null=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(TodoList, related_name="items", on_delete=CASCADE)

    def __str__(self):
        return self.task
