from django.shortcuts import render
from django.shortcuts import get_object_or_404
from todos.forms import TodoForm
from .models import TodoList, TodoItem
from django.shortcuts import render, redirect
from .forms import ItemForm, TodoForm


def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {"todolists": todolists}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, pk):
    todolist = TodoList.objects.get(pk=pk)
    context = {"todolist": todolist}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    context = {}

    form = TodoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(todo_list_list)
    context["form"] = form
    return render(request, "todos/create.html", context)


def todo_list_update(request, pk):
    context = {}

    obj = get_object_or_404(TodoList, pk=pk)
    form = TodoForm(request.POST or None, instance=obj)

    if form.is_valid():
        form.save()
        return redirect(todo_list_list)

    context["form"] = form

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, pk):
    context = {}
    obj = get_object_or_404(TodoList, pk=pk)

    if request.method == "POST":
        obj.delete()

        return redirect(todo_list_list)
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    context = {}

    form = ItemForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(todo_list_list)

    context["form"] = form
    return render(request, "todos/itemcreate.html", context)


def todo_item_update(request, pk):
    context = {}

    obj = get_object_or_404(TodoItem, pk=pk)
    form = ItemForm(request.POST or None, instance=obj)

    if form.is_valid():
        form.save()
        return redirect(todo_list_list)

    context["form"] = form

    return render(request, "todos/itemedit.html", context)
